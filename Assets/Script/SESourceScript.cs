﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SESourceScript : MonoBehaviour {

    void Awake()
    {

        if (GameObject.FindGameObjectsWithTag("SESource").Length > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            if(SceneManager.GetActiveScene().name!="_CreateStage")
                DontDestroyOnLoad(gameObject);

            SEPlayer.SetAudioSource(gameObject.GetComponent<AudioSource>());
            SEPlayer.SetSE(Resources.Load("Data/SE_Data") as SEData);
        }
    }
}
