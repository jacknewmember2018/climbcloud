﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    enum State{
        Walk,
        Jamp
    }

    State state;
	Rigidbody2D rigid2D;
	Animator animator;
	float jumpForce = 680.0f;
	float walkForce = 30.0f;
	float maxWalkSpeed = 2.0f;

	void Start () {
		this.rigid2D = GetComponent<Rigidbody2D> ();
		this.animator = GetComponent<Animator> ();
        this.state = State.Walk;
	}
	
	void Update () {
        // エンターキーでリトライ（デバッグ用）
        if (Input.GetButtonDown("Reset")){
            SceneManager.LoadScene("GameScene");
        }

        // 状態管理
        switch (state){
            case State.Walk:
                WalkState();
                break;
            case State.Jamp:
                JampState();
                break;
            default:
                break;
        }

        // 落下したらゲームオーバー
		if (transform.position.y < -10) {
            GameOver();
		}
    }

    ///************************************************************///
    // Update(各状態)
    void WalkState(){
        // ジャンプ
        if (Input.GetButtonDown("Jump"))
        {
            this.rigid2D.AddForce(transform.up * this.jumpForce);
            this.state = State.Jamp;
        }

        // 左右入力
        float input = Input.GetAxis("Horizontal");
        int key = 0;
        if (input > 0) key = 1;
        else if (input < 0) key = -1;

        float speedx = Mathf.Abs(this.rigid2D.velocity.x);

        if (speedx < this.maxWalkSpeed)
        {
            this.rigid2D.AddForce(transform.right * key * this.walkForce);
        }

        // 画像の向きを変更
        if (key != 0)
        {
            transform.localScale = new Vector3(key, 1, 1);
        }

        // アニメーションの速度を調整
        this.animator.speed = speedx / 2.0f;
    }

    void JampState(){
        // アニメーションの速度を調整
        this.animator.speed = 0;
    }
    ///************************************************************///

    ///************************************************************///
    // 接触判定
    void OnCollisionEnter2D(Collision2D collision)
    {
        // 着地
        foreach (ContactPoint2D contact in collision.contacts)
        {
            if (contact.normal.y > 0.8)
            {
                this.state = State.Walk;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        //　ゴールに到達
        if (other.gameObject.tag == "Finish"){
            GameClear();
        }
	}
    ///************************************************************///

    // ゲームクリア
    void GameClear(){
        SceneManager.LoadScene("ClearScene");
    }

    // ゲームオーバー
    void GameOver(){
        SceneManager.LoadScene("GameOverScene");
    }
}
