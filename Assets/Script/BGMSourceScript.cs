﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMSourceScript : MonoBehaviour {
    

	void Awake()
    {
        if (GameObject.FindGameObjectsWithTag("BGMSource").Length > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            BGMPlayer.SetAudioSource(gameObject.GetComponent<AudioSource>());
            BGMPlayer.SetBGM(Resources.Load("Data/BGM_Data") as BGMData);
        }
    }
}
