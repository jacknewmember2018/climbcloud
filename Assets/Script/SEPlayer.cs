﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SEPlayer{
    static AudioSource audioSource;
    static SEData SE_Data;

    public static void SetAudioSource(AudioSource source)
    {
        audioSource = source;
    }

    public static void SetSE(SEData data)
    {
        SE_Data = data;
    }

    public static void PlaySE(string keyname)
    {
        if (IsPlaying() == true)
        {
            StopSE();
        }
        audioSource.clip = SE_Data.SEList[keyname];
        audioSource.Play();
    }

    public static void StopSE()
    {
        audioSource.Stop();
    }

    public static bool IsPlaying()
    {
        return audioSource.isPlaying;
    }
}
