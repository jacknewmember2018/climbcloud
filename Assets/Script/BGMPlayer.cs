﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BGMPlayer{
    static AudioSource audioSource;
    static BGMData BGM_Data;

    public static void SetAudioSource(AudioSource source)
    {
        audioSource = source;
    }

    public static void SetBGM(BGMData data)
    {
        BGM_Data = data;
    }

    //BGM再生(音量なし)
    public static void PlayBGM(string keyname)
    {
        if (IsPlaying() == false)//シーン再読み込みのために消さない
        {
            audioSource.clip = BGM_Data.BGMList[keyname];
            audioSource.Play();
        }
    }

    //BGM再生(音量あり)
    public static void PlayBGM(string keyname,float volume)
    {
        PlayBGM(keyname);
        audioSource.volume = volume;
    }

    public static void StopBGM()
    {
        audioSource.Stop();
    }

    public static bool IsPlaying()
    {
        return audioSource.isPlaying;
    }

}
