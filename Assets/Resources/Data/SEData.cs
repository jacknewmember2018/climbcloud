﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SEData : ScriptableObject,ISerializationCallbackReceiver {
    [SerializeField]
    List<SE> SEsource=new List<SE>();
    public Dictionary<string, AudioClip> SEList=new Dictionary<string,AudioClip>();

    public void OnAfterDeserialize()
    {
        SEList = new Dictionary<string, AudioClip>();
        foreach(SE item in SEsource)
        {
            if (SEList.ContainsKey(item.SEname) == false)
            {
                SEList.Add(item.SEname, item.audioClip);
            }
        }
    }

    public void OnBeforeSerialize()
    {

    }

}

[System.Serializable]
public class SE
{
    public string SEname;
    public AudioClip audioClip;
}