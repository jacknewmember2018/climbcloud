﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BGMData : ScriptableObject, ISerializationCallbackReceiver
{
    [SerializeField]
    List<BGM> BGMsource = new List<BGM>();
    public Dictionary<string, AudioClip> BGMList = new Dictionary<string, AudioClip>();

    public void OnAfterDeserialize()
    {
        BGMList = new Dictionary<string, AudioClip>();
        foreach (BGM item in BGMsource)
        {
            if (BGMList.ContainsKey(item.BGMname) == false)
            {
                BGMList.Add(item.BGMname, item.audioClip);
            }
        }
    }

    public void OnBeforeSerialize()
    {

    }

}

[System.Serializable]
public class BGM
{
    public string BGMname;
    public AudioClip audioClip;
}
